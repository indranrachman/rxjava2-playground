package com.rachman.indra.rxtest

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import io.reactivex.subscribers.TestSubscriber
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun takeUntilTest() {
        val observer = TestObserver<Int>()

        Observable.just(1, 2, 3, 4, 5)
            .takeUntil { it == 4 }
            .subscribe(observer)

        observer.apply {
            assertSubscribed()
            assertValueCount(4)
            assertNoErrors()
            assertComplete()
        }

    }

    @Test
    fun takeWhileTest() {

        Observable.just(1, 2, 3, 4, 5)
            .takeWhile {
                it < 5
            }
            .test()
            .apply {
                assertValueAt(0, 1)
                assertValueAt(1, 2)
                assertValueAt(2, 3)
                assertValueAt(3, 4)
                assertValueCount(4)
                assertSubscribed()
                assertComplete()
            }

    }

    @Test
    fun skipWhileTest() {
        Observable.just(1)
            .flatMap {
                Observable.just(true, false)
            }
            .takeUntil {
                !it
            }
            .skipWhile { it }
            .test()
            .apply {
                assertValueCount(1)
            }

    }

    @Test
    fun nestingComplete() {
        val getNumbers = Observable.just(1, 2, 3, 4, 5)
        val saveNumbers = Completable
            .create {
                it.onComplete()
            }

        getNumbers
            .flatMapCompletable {
                saveNumbers
            }
            .test()
            .apply {
                assertSubscribed()
                assertComplete()
            }
    }


    @Test
    fun filter() {
        val observable = Observable.just(1, 2, 3, 5).filter { it == 0 }

        val test = TestObserver<Int>()

        observable.subscribe(test)

        test.assertNoValues()
        test.assertValueCount(0)
        test.assertComplete()
    }

    @Test
    fun deepNestingErrorToComplete() {
        val getNumbers = Observable.just(1, 2, 3, 4, 5)
        val saveNumbers = Completable.create {
            it.onComplete()
        }
        val saveName = Completable.create {
            it.onError(Throwable("Error saveName"))
        }

        val saveMyMonday = Completable.create {
            it.onError(Throwable("Error saveMyMonday"))
        }

        getNumbers
            .flatMapCompletable {
                saveNumbers
                    .andThen(saveName)
                    .andThen(saveMyMonday)
            }
            .onErrorComplete {
                it.message == "Error saveName"
            }
            .test()
            .apply {
                assertSubscribed()
                assertComplete()
            }

    }

    @Test
    fun deepNestingError() {
        val getNumbers = Observable.just(1, 2, 3, 4, 5)
        val saveNumbers = Completable.create {
            it.onComplete()
        }
        val saveName = Completable.create {
            it.onComplete()
        }

        val saveMyMonday = Completable.create {
            it.onError(Throwable("Error Rahasia"))
        }

        getNumbers
            .flatMapCompletable {
                saveNumbers
                    .andThen(saveName)
                    .andThen(saveMyMonday)
            }
            .test()
            .apply {
                assertSubscribed()
                assertError {
                    it.message == "Error Rahasia"
                }
                assertNotComplete()
            }

    }

    @Test
    fun nestingError() {
        val getNumbers = Observable.just(1, 2, 3, 4, 5)
        val saveNumbers = Completable.create {
            it.onError(Throwable("Error Rahasia"))
        }
        getNumbers
            .flatMapCompletable {
                saveNumbers
            }
            .test()
            .apply {
                assertSubscribed()
                assertError {
                    it.message == "Error Rahasia"
                }
                assertNotComplete()
            }

    }

    @Test
    fun testMapFilter() {
        val getNumbers = Observable.just(1, 2, 3, 4, 5)

        getNumbers
            .map { it - 5 }
            .filter { it < 0 }
            .test()
            .apply {
                assertSubscribed()
                assertValueCount(4)
                assertComplete()
            }
    }

}